export interface levelConfig {
    name: string;
    boardConfig: {
        tiles: number[][];
    };
    snakeConfig: {
        parts: {
            x: number;
            y: number;
        }[];
        interval: {
            initial: number;
            accelerateMultiplier: number;
            accelerateEvery: number;
            minimum: number;
        };
    };
}

const levelConfigs: levelConfig[] = [
    {
        name: 'Classic',
        boardConfig: {
            tiles: [
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            ]
        }, 
        snakeConfig: {
            parts: [
                { x: 1, y: 7 },
                { x: 1, y: 8 },
                { x: 1, y: 9 },
                { x: 1, y: 10 },
            ],
            interval: {
                initial: 0.3,
                accelerateMultiplier: 0.9,
                accelerateEvery: 2,
                minimum: 0.12,
            }
        }
    },
    {
        name: 'Bird-Eye',
        boardConfig: {
            tiles: [
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 0],
                [0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0],
                [0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1],
                [0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1],
                [0, 1, 1, 1, 0, 1, 0, 1, 0, 0, 1, 1],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0],
                [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 1, 0, 1, 0, 1, 1, 0, 0, 1, 1, 0],
                [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 1, 0, 0, 0, 1, 1, 1, 0, 1, 1, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            ]
        }, 
        snakeConfig: {
            parts: [
                { x: 9, y: 5 },
                { x: 9, y: 4 },
                { x: 10, y: 4 },
                { x: 10, y: 3 },
                { x: 10, y: 2 },
                { x: 11, y: 2 },
                { x: 11, y: 1 },
            ],
            interval: {
                initial: 0.3,
                accelerateMultiplier: 0.9,
                accelerateEvery: 10,
                minimum: 0.2,
            }
        }
    },
    {
        name: 'Naruto',
        boardConfig: {
            tiles: [
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
                [0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 1, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
                [0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0],
                [0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0],
                [0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0],
                [0, 0, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0],
                [0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0],
                [0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0],
                [0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            ]
        }, 
        snakeConfig: {
            parts: [
                { x: 4, y: 7 },
                { x: 5, y: 7 },
                { x: 5, y: 6 },
                { x: 4, y: 6 },
                { x: 3, y: 6 },
                { x: 3, y: 7 },
            ],
            interval: {
                initial: 0.3,
                accelerateMultiplier: 0.9,
                accelerateEvery: 5,
                minimum: 0.2,
            }
        }
    },
    {
        name: 'Invalid',
        boardConfig: {
            tiles: [
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            ]
        }, 
        snakeConfig: {
            parts: [
                { x: 1, y: 7 },
                { x: 1, y: 8 },
                { x: 1, y: 9 },
                { x: 1, y: 10 },
            ],
            interval: {
                initial: 0.3,
                accelerateMultiplier: 0.9,
                accelerateEvery: 2,
                minimum: 0.12,
            }
        }
    }
]

const levelToRandom = [0, 1 , 2];

function getRandomLevel() {
    return levelConfigs[0];
    return levelConfigs[
        levelToRandom[Math.floor(Math.random() * levelToRandom.length)]
    ];
}

export function getLevelConfig() {
    return getRandomLevel();
}
