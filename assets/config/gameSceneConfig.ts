import { Vec3, math } from "cc";

export function getTilePosition(col: number, row: number) {
    const firstTilePosition = new Vec3(-147.5, 163.5, 0);
    return {
        x: firstTilePosition.x + col * getTileSize().width,
        y: firstTilePosition.y - row * getTileSize().height,
    }
}

export function getTileSize() {
    return math.size(27, 27);
}