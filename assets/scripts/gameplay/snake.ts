import {
  _decorator,
  Component,
  Node,
  Sprite,
  UITransform,
  Vec2,
  Prefab,
} from "cc";
import { PlayerInput, MoveDirection } from "./playerInput";
import { GameEvent } from "../helpers/gameEvent";
import { SnakeHead } from "../sprites/snakeHead";
import { getTileSize, getTilePosition } from "../../config/gameSceneConfig";
import { SnakeBody } from "../sprites/snakeBody";
import { SnakeTail } from "../sprites/snakeTail";
import { SnakeBelly } from "../sprites/snakeBelly";
const { ccclass, property } = _decorator;

@ccclass("Snake")
export class Snake extends Component {
  @property({ type: PlayerInput })
  playerInput?: PlayerInput;

  @property({ type: Node })
  playArea?: Node;

  private snakeConfig?: {
    parts: { x: number; y: number }[];
    interval: {
      initial: number;
      accelerateMultiplier: number;
      accelerateEvery: number;
      minimum: number;
    };
  };

  private currentSpeed: number = 0;

  private snake: { part: Node; index: Vec2 }[] = [];

  private initialSnakeLength = 0;

  private snakeFacing?: MoveDirection;

  private isQueueMoving = false;

  private onSnakeMoved?: (index: Vec2[]) => void;

  private onSnakeTurn?: (direction: MoveDirection) => void;

  private snakeMoveLoop!: () => void;

  private addedBodyCount: number = 0;

  private consumedFoodPos: Vec2[] = [];

  init(
    snakeConfig: {
      parts: { x: number; y: number }[];
      interval: {
        initial: number;
        accelerateMultiplier: number;
        accelerateEvery: number;
        minimum: number;
      };
    },
    onSnakeMoved: (index: Vec2[]) => void,
    onSnakeTurn: (direction: MoveDirection) => void
  ) {
    this.snakeConfig = snakeConfig;
    this.listenToPlayerInput();
    this.createSnake();

    this.onSnakeMoved = onSnakeMoved;
    this.onSnakeTurn = onSnakeTurn;
  }

  createSnake() {
    if (this.snakeConfig) {
      const snekPos = this.snakeConfig.parts;
      for (let i = 0; i < snekPos.length; i++) {
        const snekPart = new Node("SnekPart");
        snekPart.setParent(this.playArea!);
        snekPart.layer = this.playArea!.layer;
        snekPart.addComponent(Sprite);

        if (i === 0) {
          snekPart.name = "Hed";
          snekPart.addComponent(SnakeHead);
        } else if (i > 0 && i < snekPos.length - 1) {
          snekPart.name = "Body";
          snekPart.addComponent(SnakeBody);
        } else {
          snekPart.name = "Tail";
          snekPart.addComponent(SnakeTail);
        }
        const snekPartPos = getTilePosition(snekPos[i].x, snekPos[i].y);
        snekPart.setPosition(snekPartPos.x, snekPartPos.y);

        this.snake.push({
          part: snekPart,
          index: new Vec2(snekPos[i].x, snekPos[i].y),
        });

        this.initialSnakeLength = this.snake.length;
      }

      for (let i = 0; i < snekPos.length; i++) {
        if (i !== snekPos.length - 1) {
          const front = snekPos[i];
          const back = snekPos[i + 1];
          const dir = new Vec2(front.x - back.x, front.y - back.y);
          this.snake[i].part.angle = this.getAngleFromVectorDirection(dir);
          if (i === 0) {
            this.setSnakeFacing(dir);
          }
        } else {
          const front = snekPos[i - 1];
          const tail = snekPos[i];
          const dir = new Vec2(front.x - tail.x, front.y - tail.y);
          this.snake[i].part.angle = this.getAngleFromVectorDirection(dir);
        }
      }
    }
  }

  createTail(posIndex: Vec2) {
    const snekPart = new Node("Appendage");
    snekPart.setParent(this.playArea!);
    snekPart.layer = this.playArea!.layer;
    snekPart.addComponent(Sprite);
    snekPart.addComponent(SnakeTail);
    const snekPartPos = getTilePosition(posIndex.x, posIndex.y);
    snekPart.setPosition(snekPartPos.x, snekPartPos.y);

    return snekPart;
  }

  swallow(foodPos: Vec2) {
    this.addedBodyCount++;
    this.consumedFoodPos.push(foodPos);
    if (this.shouldIncreaseSpeed()) {
      this.stopMoving();
      this.increaseSpeed();
      this.startMoving();
    }
  }

  private checkForAppendage() {
    this.consumedFoodPos.forEach((foodPos) => {
      const tail = this.snake[this.snake.length - 1];
      if (foodPos.x === tail.index.x && foodPos.y === tail.index.y) {
        this.AppendSnake(tail, foodPos);
      }
    });
  }

  private AppendSnake(tail: { part: Node; index: Vec2; }, foodPos: Vec2) {
    tail.part.getComponent(SnakeTail)!.destroy();
    tail.part.addComponent(SnakeBody);
    this.snake.push({
      index: foodPos,
      part: this.createTail(foodPos),
    });
    const index = this.consumedFoodPos.indexOf(foodPos, 0);
    if (index > -1) {
      this.consumedFoodPos.splice(index, 1);
    }
  }

  private shouldIncreaseSpeed() {
    const { interval } = this.snakeConfig!;

    return this.addedBodyCount % interval.accelerateEvery === 0;
  }

  private increaseSpeed() {
    const { interval } = this.snakeConfig!;

    if (this.currentSpeed > interval.minimum) {
      this.currentSpeed *= interval.accelerateMultiplier;
    } else {
      this.currentSpeed = interval.minimum;
    }
  }

  stopMoving() {
    this.unschedule(this.snakeMoveLoop);
  }

  getCurrentPosition() {
    const posIndex = this.snake.map((part) => part.index);

    return posIndex;
  }

  private move(direction: MoveDirection) {
    if (this.isMoveValid(direction) && !this.isQueueMoving) {
      this.isQueueMoving = true;
      const moveDir = this.getVectorDirectionFromMoveDirection(direction);

      this.setSnakeFacing(moveDir);
    } 

    if (this.currentSpeed === 0) {
      this.currentSpeed = this.snakeConfig!.interval.initial;
      this.startMoving();
    }
  }

  private startMoving() {
    this.snakeMovement(this.currentSpeed);
  }

  private setSnakeFacing(dir: Vec2) {
    if (dir.x === -1) this.snakeFacing = MoveDirection.Left;
    else if (dir.x === 1) this.snakeFacing = MoveDirection.Right;
    else if (dir.y === 1) this.snakeFacing = MoveDirection.Down;
    else this.snakeFacing = MoveDirection.Up;

    this.onSnakeTurn?.(this.snakeFacing);
  }

  private listenToPlayerInput() {
    if (this.playerInput) {
      this.playerInput.node.on(GameEvent.Move, this.move, this);
    }
  }

  private snakeMovement(speed: number) {
    this.snakeMoveLoop = () => {
      this.animateSnakeMovement();
      this.isQueueMoving = false;
    };
    this.schedule(this.snakeMoveLoop, speed);
  }

  private animateSnakeMovement() {
    const direction = this.getVectorDirectionFromMoveDirection(
      this.snakeFacing!
    );
    let prevPos = this.snake[0].index;
    for (let i = 0; i < this.snake.length; i++) {
      const frontPart = new Vec2(this.snake[i].index.x, this.snake[i].index.y);

      // Move the head to our direction
      if (i === 0) {
        this.snake[i].index.x += direction.x;
        this.snake[i].index.y += direction.y;
        this.snake[i].part.angle = this.getAngleFromVectorDirection(direction);
      } else {
        // follow the front part of snek.
        this.snake[i].index.x = prevPos.x;
        this.snake[i].index.y = prevPos.y;
        this.snake[i].part.angle = this.getAngleFromVectorDirection(
          new Vec2(
            this.snake[i - 1].index.x - prevPos.x,
            this.snake[i - 1].index.y - prevPos.y
          )
        );
      }

      prevPos = frontPart;
    }

    this.snake.forEach((part) => {
      const pos = getTilePosition(part.index.x, part.index.y);
      part.part.setPosition(pos.x, pos.y);
    });

    const snakeIndex = this.snake.map((part) => part.index);
    this.onSnakeMoved?.(snakeIndex);
    this.checkForAppendage();
  }

  private isMoveValid(direction: MoveDirection) {
    if (
      this.snakeFacing === MoveDirection.Left ||
      this.snakeFacing === MoveDirection.Right
    ) {
      if (direction === MoveDirection.Up || direction === MoveDirection.Down) {
        return true;
      }
    } else if (
      this.snakeFacing === MoveDirection.Up ||
      this.snakeFacing === MoveDirection.Down
    ) {
      if (direction === MoveDirection.Left || direction === MoveDirection.Right)
        return true;
    }

    return false;
  }

  private getVectorDirectionFromMoveDirection(direction: MoveDirection) {
    const vectorDirection = new Vec2(0, 0);
    switch (direction) {
      case MoveDirection.Left:
        vectorDirection.x = -1;
        break;
      case MoveDirection.Up:
        vectorDirection.y = -1;
        break;
      case MoveDirection.Down:
        vectorDirection.y = 1;
        break;
      case MoveDirection.Right:
        vectorDirection.x = 1;
        break;
    }

    return vectorDirection;
  }

  private getAngleFromVectorDirection(direction: Vec2) {
    if (direction.x === -1) {
      return 90;
    } else if (direction.x === 1) {
      return 270;
    } else if (direction.y === 1) {
      return 180;
    } else {
      return 0;
    }
  }
}
