import {
  _decorator,
  Component,
  Node,
  assetManager,
  AudioClip,
  AudioSource,
} from "cc";
import { AudioKey } from "../helpers/assetKey";
const { ccclass, property } = _decorator;

@ccclass("SfxManager")
export class SfxManager extends Component {
  sfxSource?: AudioSource | null;

  private turnClip!: AudioClip;
  private deadClip!: AudioClip;
  private eatClip!: AudioClip;

  start() {
    this.sfxSource = this.node.getComponent(AudioSource);

    this.turnClip = assetManager.assets.get(AudioKey.Turn) as AudioClip;

    this.deadClip = assetManager.assets.get(AudioKey.Dead) as AudioClip;

    this.eatClip = assetManager.assets.get(AudioKey.Eat) as AudioClip;
  }

  playSfx(key: AudioKey) {
    switch (key) {
      case AudioKey.Turn:
        this.sfxSource!.playOneShot(this.turnClip);
        break;
      case AudioKey.Dead:
        this.sfxSource!.playOneShot(this.deadClip);
        break;
      case AudioKey.Eat:
        this.sfxSource!.playOneShot(this.eatClip);
        break;
    }
  }
}
