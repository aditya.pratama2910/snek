import {
  _decorator,
  Node,
  Prefab,
  instantiate,
  UITransform,
} from "cc";
import { YellowTile } from "../sprites/yellowTile";
import { OrangeTile } from "../sprites/orangeTile";
import { getTilePosition, getTileSize } from "../../config/gameSceneConfig";
import { WallTile } from "../sprites/wallTile";
import { levelConfig } from "../../config/maps";

export class MapGenerator {
  static createMap(levelConfig: levelConfig, tilePrefab: Prefab, rootTileNode: Node) {
    const tileArray: Node[][] = [];
    const { tiles } = levelConfig.boardConfig;
    for (let row = 0; row < tiles.length; row++) {
        tileArray.push([]);
      for (let col = 0; col < tiles[row].length; col++) {
        const tile = tiles[row][col];
        const tileObject = instantiate(tilePrefab);
        if (tileObject) {
          tileObject.setParent(rootTileNode);

          const tilePos = getTilePosition(col, row);
          tileObject.setPosition(tilePos.x, tilePos.y);
        }

        if(tile === 0) {
            if ((row + col) % 2 === 0) {
                tileObject.addComponent(YellowTile);
              } else {
                tileObject.addComponent(OrangeTile);
              }
        }
        else if(tile === 1) {
            tileObject.addComponent(WallTile);
        }

        tileObject
          .getComponent(UITransform)
          ?.setContentSize(getTileSize());

        tileArray[row].push(tileObject);
      }
    }

    return tileArray;
  }
}
