import {
  _decorator,
  Component,
  Node,
  Prefab,
  Vec2,
  Sprite,
  UITransform,
  Label,
  AudioSource,
} from "cc";
import { getLevelConfig, levelConfig } from "../../config/maps";
import { MapGenerator } from "./mapGenerator";
import { Snake } from "./snake";
import { WallTile } from "../sprites/wallTile";
import { Fruit } from "../sprites/fruit";
import { getTileSize } from "../../config/gameSceneConfig";
import { Highscore } from "../highscore";
import { SfxManager } from "./sfxManager";
import { AudioKey } from "../helpers/assetKey";
import { MoveDirection } from "./playerInput";
import { ResultPopup } from "../resultPopup";
const { ccclass, property } = _decorator;

@ccclass("GameManager")
export class GameManager extends Component {
  @property({ type: SfxManager })
  sfxManager?: SfxManager;

  @property({ type: Prefab })
  tilePrefab?: Prefab;

  @property({ type: Node })
  tileParent?: Node;

  @property({ type: Snake })
  snek?: Snake;

  @property({ type: ResultPopup })
  resultPopup?: ResultPopup;

  @property({ type: Label })
  scoreText?: Label;

  @property({ type: Highscore })
  highscoreText?: Highscore;

  private fruit?: Fruit;

  readonly levelConfig: levelConfig;
  private tileArray?: Node[][];
  private walkable: Map<string, Vec2> = new Map<string, Vec2>();

  private score = 0;

  constructor() {
    super("GameManager");
    this.levelConfig = getLevelConfig();
  }

  start() {
    this.tileArray = MapGenerator.createMap(
      this.levelConfig,
      this.tilePrefab!,
      this.tileParent!
    );

    const { tiles } = this.levelConfig.boardConfig;
    for (let y = 0; y < tiles.length; y++) {
      for (let x = 0; x < tiles[y].length; x++) {
        if (tiles[y][x] === 0) {
          this.walkable.set(`${x}${y}`, new Vec2(x, y));
        }
      }
    }

    if (this.isSnakeValid()) {
      this.snek?.init(
        this.levelConfig.snakeConfig,
        this.onSnakeMove.bind(this),
        this.onSnakeTurn.bind(this)
      );
    } else {
      console.log("invalid snake");
      // TODO: create popup.
    }

    this.fruit = this.spawnFruit();
  }

  private increaseScore() {
    this.score++;
    if (this.scoreText) {
      this.scoreText.string = this.score.toString();
    }

    if (this.score > this.highscoreText!.getHighscore()) {
      this.highscoreText!.setHighscore(this.score);
    }
  }

  private gameOver() {
    this.sfxManager?.playSfx(AudioKey.Dead);
    this.snek?.stopMoving();

    this.resultPopup?.show(this.score);
  }

  private spawnFruit() {
    const fruitNode = new Node("FrutsU!");
    fruitNode.setParent(this.tileParent!);
    fruitNode.layer = this.tileParent!.layer;
    fruitNode.addComponent(Sprite);
    fruitNode.addComponent(Fruit).setPosition(this.getFruitRandomPosition());
    fruitNode.addComponent(UITransform).setContentSize(getTileSize());

    return fruitNode.getComponent(Fruit)!;
  }

  private getFruitRandomPosition() {
    const snakePos = this.snek?.getCurrentPosition();
    const walkableCopy = new Map<string, Vec2>(this.walkable);

    if (snakePos) {
      snakePos.forEach((pos) => {
        walkableCopy.delete(`${pos.x}${pos.y}`);
      });
    }

    let index = Math.floor(Math.random() * walkableCopy.size);
    let i = 0;
    let ret = "";
    for (let key of walkableCopy.keys()) {
      if (i++ === index) {
        ret = key;
        break;
      }
    }

    const vector = walkableCopy.get(ret)!;
    const v = new Vec2(vector.x, vector.y);
    return v;
  }

  private onSnakeMove(snakeIndex: Vec2[]) {
    const headIndex = snakeIndex[0];
    try {
      if (this.tileArray![headIndex.y][headIndex.x].getComponent(WallTile)) {
        // Wall.
        this.gameOver();
      }

      // Nom nom.
      if (
        headIndex.x === this.fruit?.getPosition()?.x &&
        headIndex.y === this.fruit?.getPosition()?.y
      ) {
        this.snek?.swallow(this.fruit.getPosition()!);
        this.fruit?.node.destroy();
        this.increaseScore();
        this.fruit = this.spawnFruit();
        this.sfxManager?.playSfx(AudioKey.Eat);
      }

      // Cannibal.
      if (this.isSnakeCannibal(snakeIndex)) {
        this.gameOver();
      }
    } catch (e) {
      // Out of range.
      this.gameOver();
    }
  }

  private onSnakeTurn(direction: MoveDirection) {
    this.sfxManager?.playSfx(AudioKey.Turn);
  }

  private isSnakeValid() {
    // Snake on wall.
    const { parts } = this.levelConfig.snakeConfig;
    parts.forEach((part) => {
      const wall = this.tileArray![part.y][part.x].getComponent(WallTile);
      if (wall) {
        return false;
      }
    });

    // Snake length < 3
    if (this.levelConfig.snakeConfig.parts.length < 3) return false;

    // TODO: snake part dist != 1

    return true;
  }

  private isSnakeCannibal(snakeIndex: Vec2[]) {
    const snekMap = new Set<string>();

    snakeIndex.forEach((snek) => snekMap.add(`${snek.x}:${snek.y}`));
    if (snekMap.size != snakeIndex.length) {
      return true;
    } else {
      return false;
    }
  }
}
