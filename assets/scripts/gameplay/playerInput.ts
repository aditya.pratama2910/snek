import {
  _decorator,
  Component,
  Node,
  systemEvent,
  SystemEvent,
  EventKeyboard,
  macro,
} from "cc";
import { GameEvent } from "../helpers/gameEvent";
const { ccclass } = _decorator;

export enum MoveDirection{
    Left,
    Up,
    Down,
    Right
};

@ccclass("PlayerInput")
export class PlayerInput extends Component {

  start() {
    systemEvent.on(SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
  }

  private onKeyDown(event: EventKeyboard) {
    switch (event.keyCode) {
      case macro.KEY.left:
        this.onPlayerMoveLeft();
        break;
      case macro.KEY.up:
        this.onPlayerMoveUp();
        break;
      case macro.KEY.down:
        this.onPlayerMoveDown();
        break;
      case macro.KEY.right:
        this.onPlayerMoveRight();
        break;
    }
  }

  onPlayerMoveLeft() {
    this.node.emit(GameEvent.Move, MoveDirection.Left);
  }

  onPlayerMoveUp() {
    this.node.emit(GameEvent.Move, MoveDirection.Up);
  }

  onPlayerMoveDown() {
    this.node.emit(GameEvent.Move, MoveDirection.Down);
  }

  onPlayerMoveRight() {
    this.node.emit(GameEvent.Move, MoveDirection.Right);
  }
}