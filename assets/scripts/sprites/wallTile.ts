
import { _decorator } from 'cc';
import { BaseSprite } from '../base/baseSprite';
import { ImageKey } from '../helpers/assetKey';
const { ccclass } = _decorator;

@ccclass('WallTile')
export class WallTile extends BaseSprite {
    constructor() {
        super(ImageKey.Wall);
    }
}