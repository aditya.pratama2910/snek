
import { _decorator, UITransform, CCInteger } from 'cc';
import { BaseSprite } from '../base/baseSprite';
import { getSpriteFrameKey, SpriteKey } from '../helpers/assetKey';
import { PlayerInput } from '../gameplay/playerInput';
const { ccclass, property } = _decorator;

@ccclass('KeypadLeft')
export class KeypadLeft extends BaseSprite {
    @property({type: PlayerInput})
    private playerInput?: PlayerInput;
    

    constructor() {
        super(getSpriteFrameKey(SpriteKey.Keypad, 3));
    }

    set() {
        super.set();
        this.node.addComponent(UITransform).setContentSize(65, 65);
        this.node.on('click', this.onClick, this);
    }

    onClick() {
        this.playerInput?.onPlayerMoveLeft();
    }
}