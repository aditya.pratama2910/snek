
import { _decorator } from 'cc';
import { BaseSprite } from '../base/baseSprite';
import { getSpriteFrameKey, SpriteKey } from '../helpers/assetKey';
const { ccclass } = _decorator;

@ccclass('YellowTile')
export class YellowTile extends BaseSprite {
    constructor() {
        super(getSpriteFrameKey(SpriteKey.Tile, 0));
    }
}