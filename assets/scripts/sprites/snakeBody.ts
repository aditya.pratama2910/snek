
import { _decorator, UITransform } from 'cc';
import { BaseSprite } from '../base/baseSprite';
import { getSpriteFrameKey, SpriteKey } from '../helpers/assetKey';
import { getTileSize } from '../../config/gameSceneConfig';
const { ccclass } = _decorator;

@ccclass('SnakeBody')
export class SnakeBody extends BaseSprite {
    constructor() {
        super(getSpriteFrameKey(SpriteKey.Snek, 3));
    }

    set() {
        super.set();
        this.node.addComponent(UITransform).setContentSize(getTileSize());
    }
}