
import { _decorator, SystemEvent, Toggle } from 'cc';
import { BaseSprite } from '../base/baseSprite';
import { ImageKey } from '../helpers/assetKey';
const { ccclass } = _decorator;

@ccclass('Unmute')
export class Unmute extends BaseSprite {
    constructor() {
        super(ImageKey.SoundOn);
    }
}