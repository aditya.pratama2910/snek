
import { _decorator } from 'cc';
import { ImageKey } from '../helpers/assetKey';
import { BaseSprite } from '../base/baseSprite';
const { ccclass } = _decorator;

@ccclass('ScoreIcon')
export class ScoreIcon extends BaseSprite {
    constructor() {
        super(ImageKey.Apple);
    }
}