
import { _decorator, UITransform, CCInteger, systemEvent, System, SystemEvent } from 'cc';
import { BaseSprite } from '../base/baseSprite';
import { getSpriteFrameKey, SpriteKey } from '../helpers/assetKey';
import { PlayerInput } from '../gameplay/playerInput';
const { ccclass, property } = _decorator;

@ccclass('KeypadDown')
export class KeypadDown extends BaseSprite {
    @property({type: PlayerInput})
    private playerInput?: PlayerInput;
    
    constructor() {
        super(getSpriteFrameKey(SpriteKey.Keypad, 4));
    }

    set() {
        super.set();
        this.node.addComponent(UITransform).setContentSize(65, 65);
        this.node.on('click', this.onClick, this);
    }

    onClick() {
        this.playerInput?.onPlayerMoveDown();
    }
}