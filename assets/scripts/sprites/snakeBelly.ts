
import { _decorator, UITransform } from 'cc';
import { BaseSprite } from '../base/baseSprite';
import { getSpriteFrameKey, SpriteKey } from '../helpers/assetKey';
import { getTileSize } from '../../config/gameSceneConfig';
const { ccclass } = _decorator;

@ccclass('SnakeBelly')
export class SnakeBelly extends BaseSprite {
    constructor() {
        super(getSpriteFrameKey(SpriteKey.Snek, 1));
    }

    set() {
        super.set();
        this.node.addComponent(UITransform).setContentSize(getTileSize());
    }
}