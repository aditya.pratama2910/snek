
import { _decorator, UITransform, CCInteger } from 'cc';
import { BaseSprite } from '../base/baseSprite';
import { getSpriteFrameKey, SpriteKey } from '../helpers/assetKey';
import { PlayerInput } from '../gameplay/playerInput';
const { ccclass, property } = _decorator;

@ccclass('KeypadUp')
export class KeypadUp extends BaseSprite {
    @property({type: PlayerInput})
    private playerInput?: PlayerInput;

    constructor() {
        super(getSpriteFrameKey(SpriteKey.Keypad, 1));
    }

    set() {
        super.set();
        this.node.addComponent(UITransform).setContentSize(65, 65);
        this.node.on('click', this.onClick, this);
    }

    onClick() {
        this.playerInput?.onPlayerMoveUp();
    }
}