
import { _decorator, SystemEvent } from 'cc';
import { BaseSprite } from '../base/baseSprite';
import { ImageKey } from '../helpers/assetKey';
const { ccclass } = _decorator;

@ccclass('Mute')
export class Mute extends BaseSprite {
    constructor() {
        super(ImageKey.SoundOff);
    }
}