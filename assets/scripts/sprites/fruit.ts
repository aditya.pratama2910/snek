
import { _decorator, Vec2 } from 'cc';
import { BaseSprite } from '../base/baseSprite';
import { getSpriteFrameKey, SpriteKey, ImageKey } from '../helpers/assetKey';
import { getTilePosition } from '../../config/gameSceneConfig';
const { ccclass } = _decorator;

@ccclass('Fruit')
export class Fruit extends BaseSprite {
    private position?: Vec2;

    constructor() {
        super(ImageKey.Apple);
    }

    setPosition(index: Vec2) {
        this.position = index;
        const tilePos = getTilePosition(index.x, index.y);
        this.node.setPosition(tilePos.x, tilePos.y);
    }

    getPosition() {
        return this.position;
    }
}