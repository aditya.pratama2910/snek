
import { _decorator } from 'cc';
import { ImageKey } from '../helpers/assetKey';
import { BaseSprite } from '../base/baseSprite';
const { ccclass } = _decorator;

@ccclass('GameLogo')
export class GameLogo extends BaseSprite {   
    constructor() {
        super(ImageKey.Logo);
    }
}