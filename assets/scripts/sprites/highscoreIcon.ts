
import { _decorator } from 'cc';
import { ImageKey } from '../helpers/assetKey';
import { BaseSprite } from '../base/baseSprite';
const { ccclass } = _decorator;

@ccclass('HighscoreIcon')
export class HighscoreIcon extends BaseSprite {
    constructor() {
        super(ImageKey.Trophy);
    }
}