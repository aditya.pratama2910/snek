export enum GameEvent {
    Move = 'move',
    AssetLoadStart = 'assetLoadStart',
    AssetLoadSuccess = 'assetLoadSuccess',
    AssetLoadFail = 'assetLoadFail',
}