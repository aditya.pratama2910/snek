export class AssetKey {
  static Sprite: typeof SpriteKey;
  static Audio: typeof AudioKey;
  static Image: typeof ImageKey;
}

export enum SpriteKey {
  Snek = "snake",
  Tile = "tile",
  Keypad = "keypad",
}

export enum ImageKey {
  Logo = "logo",
  Apple = "apple",
  SoundOff = "soundOff",
  SoundOn = "soundOn",
  Trophy = "trophy",
  Wall = "wall",
}

export enum AudioKey {
  BGM = "bgm",
  ButtonClick = "btnClick",
  Dead = "dead",
  Eat = "eat",
  Turn = "turn",
}

export function getRawAssetKey(key: string) {
  return `${key}_ASSET`;
}

export function getSpriteFrameKey(
  textureKey: string,
  frameKey?: number | string
) {
  if (frameKey !== undefined) {
    return `${textureKey}_${frameKey}`;
  }
  return textureKey;
}
