import { _decorator, Component, Node, Label } from "cc";
const { ccclass } = _decorator;

@ccclass("Highscore")
export class Highscore extends Component {
  private label?: Label | null;

  start() {
    this.label = this.getComponent(Label);
    this.setText();
  }

  getHighscore() {
    let highScore = 0;
    if (localStorage.getItem("score")) {
      highScore = parseInt(localStorage.getItem("score")!);
    }
    return highScore;
  }

  setHighscore(score: number) {
    localStorage.setItem("score", score.toString());
    this.setText();
  }

  setText() {
    if (this.label) {
      this.label.string = this.getHighscore().toString();
    }
  }
}
