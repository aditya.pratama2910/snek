import {
  _decorator,
  Component,
  Node,
  resources,
  assetManager,
  ImageAsset,
  SpriteFrame,
  Label,
  Sprite,
  director,
  Texture2D,
  Rect,
} from "cc";
import { asset, AssetType, ServerAsset, AssetConfig } from "./interfaces/asset";
import { getAssets } from "./assetRecord";
import { GameEvent } from "./helpers/gameEvent";
import { getSpriteFrameKey, getRawAssetKey } from "./helpers/assetKey";
const { ccclass, property } = _decorator;

@ccclass("AssetLoader")
export class AssetLoader extends Component {
  private assetsToLoad: asset[] = [];

  private assetsLoaded = 0;

  private serverAsset: Map<string, ServerAsset> = new Map<
    string,
    ServerAsset
  >();

  @property({ type: Label })
  progressLabel?: Label;

  constructor() {
    super("AssetLoader");
    this.assetsToLoad = getAssets();
  }

  start() {
    this.loadAssets();
    this.node.on(GameEvent.AssetLoadStart, this.setProgressPercentage, this);
    this.node.on(GameEvent.AssetLoadSuccess, this.onAssetLoaded, this);
  }

  setProgressPercentage() {
    if (this.progressLabel) {
      this.progressLabel.string = ` ${this.getProgress()}% `;
    }
  }

  onAssetLoaded(key: string) {
    this.serverAsset.get(key)?.set();
    this.setProgressPercentage();

    if (this.assetsLoaded === this.assetsToLoad.length) {
      director.loadScene("title");
    }
  }

  loadAssets() {
    this.node.emit(GameEvent.AssetLoadStart);
    this.assetsToLoad.forEach((asset) => {
      resources.load(asset.localUrl, (error, result) => {
        if (error) {
          console.log(error.message);
          return;
        }
        this.assetsLoaded++;

        // Remap Asset cache key from using UUID to custom raw Key.
        this.remapAssetManagerEntry(result._uuid, getRawAssetKey(asset.key.toString()));

        this.postProcessLoadedAssets(asset);
        this.node.emit(GameEvent.AssetLoadSuccess, asset.key);
      });
    });
  }

  getProgress() {
    return (this.assetsLoaded / this.assetsToLoad.length) * 100;
  }

  addServerAsset(serverAsset: ServerAsset) {
    this.serverAsset.set(serverAsset.key, serverAsset);
  }

  removeServerAsset(serverAssetKey: string) {
    this.serverAsset.delete(serverAssetKey);
  }

  private remapAssetManagerEntry(uuid: string, key: string) {
    const existingEntry = assetManager.assets.get(uuid);

    if (!existingEntry) return;

    assetManager.assets.add(key, existingEntry);
    assetManager.assets.remove(uuid);
  }

  private postProcessLoadedAssets(asset: asset) {
    switch (asset.type) {
      case AssetType.Image:
        this.postProcessImage(getRawAssetKey(asset.key.toString()), asset.key.toString());
        break;
      case AssetType.SpriteSheet:
        if(asset.config) {
          this.postProcessSprite(getRawAssetKey(asset.key.toString()), asset.key.toString(), asset.config);
        }
        case AssetType.Audio:
        this.postProcessAudio(getRawAssetKey(asset.key.toString()), asset.key.toString());
        break;
    }
  }

  private postProcessImage(rawAssetKey: string, assetKey: string) {
    const imageAsset = assetManager.assets.get(rawAssetKey) as ImageAsset;
    if (!imageAsset) return;

    const spriteFrame = SpriteFrame.createWithImage(imageAsset);
    assetManager.assets.add(assetKey, spriteFrame);
  }

  private postProcessSprite(rawAssetKey: string, assetKey: string, config: AssetConfig) {
    const spriteAsset = assetManager.assets.get(rawAssetKey) as ImageAsset;
    if (!spriteAsset) return;

    const {width, height} = spriteAsset;
    const {frameHeight, frameWidth, paddingX, paddingY} = config;

    const texture = new Texture2D();
    texture.image = spriteAsset;

    let frame = 0;
    for(let row = 0; row < height; row += (frameHeight + paddingY)) {
      for(let col = 0; col < width; col += (frameWidth + paddingX)) {
        const spriteFrame = new SpriteFrame();
        spriteFrame.texture = texture;
        spriteFrame.rect = new Rect(col, row, frameWidth, frameHeight);
        assetManager.assets.add(getSpriteFrameKey(assetKey, frame), spriteFrame);
        frame += 1;
      }
    }
  }

  private postProcessAudio(rawAssetKey: string, assetKey: string) {
    const audioAssets = assetManager.assets.get(rawAssetKey);
    if (!audioAssets) return;

    assetManager.assets.add(assetKey, audioAssets);
  }
}
