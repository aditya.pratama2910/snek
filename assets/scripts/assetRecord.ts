import { asset, AssetType, AssetExtension } from "./interfaces/asset";
import { AssetKey, ImageKey, AudioKey, SpriteKey } from "./helpers/assetKey";

export function getAssets() {
    const assets: asset[] = [];

    assets.push({
        key: ImageKey.Logo,
        type: AssetType.Image,
        url: '',
        localUrl: 'sprites/logo_shopee_ular',
    });

    assets.push({
        key: ImageKey.SoundOff,
        type: AssetType.Image,
        url: '',
        localUrl: 'sprites/sprite_sound_off',
    });

    assets.push({
        key: ImageKey.SoundOn,
        type: AssetType.Image,
        url: '',
        localUrl: 'sprites/sprite_sound_on',
    });

    assets.push({
        key: ImageKey.Apple,
        type: AssetType.Image,
        url: '',
        localUrl: 'sprites/sprite_apple',
    });

    assets.push({
        key: SpriteKey.Tile,
        type: AssetType.SpriteSheet,
        url: '',
        localUrl: 'sprites/sprite_tile',
        config: {
            frameHeight: 48,
            frameWidth: 48,
            paddingX: 0,
            paddingY: 0,
        }
    });

    assets.push({
        key: ImageKey.Wall,
        type: AssetType.Image,
        url: '',
        localUrl: 'sprites/sprite_wall',
    });

    assets.push({
        key: ImageKey.Trophy,
        type: AssetType.Image,
        url: '',
        localUrl: 'sprites/sprite_trophy',
    });

    assets.push({
        key: SpriteKey.Snek,
        type: AssetType.SpriteSheet,
        url: '',
        localUrl: 'sprites/spritesheet_round',
        config: {
            frameHeight: 96,
            frameWidth: 96.75,
            paddingX: 0,
            paddingY: 0,
        }
    });

    assets.push({
        key: SpriteKey.Keypad,
        type: AssetType.SpriteSheet,
        url: '',
        localUrl: 'sprites/keypad',
        config: {
            frameHeight: 132,
            frameWidth: 137.4,
            paddingX: 0,
            paddingY: 0,
        }
    });

    assets.unshift({
        key: AudioKey.BGM,
        type: AssetType.Audio,
        url: '',
        localUrl: 'sfx/bg-music',
    });

    assets.push({
        key: AudioKey.ButtonClick,
        type: AssetType.Audio,
        url: '',
        localUrl: 'sfx/button-sfx',
    });

    assets.push({
        key: AudioKey.Turn,
        type: AssetType.Audio,
        url: '',
        localUrl: 'sfx/turn'
    });

    assets.push({
        key: AudioKey.Dead,
        type: AssetType.Audio,
        url: '',
        localUrl: 'sfx/crash'
    });

    assets.push({
        key: AudioKey.Eat,
        type: AssetType.Audio,
        url: '',
        localUrl: 'sfx/eat'
    });

    return assets;
}