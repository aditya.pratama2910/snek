import { SpriteKey, ImageKey, AudioKey } from "../helpers/assetKey";

export interface asset {
  key: SpriteKey | ImageKey | AudioKey;
  type: AssetType;
  url: string;
  localUrl: string;
  extension?: AssetExtension;
  config?: AssetConfig;
}

export enum AssetType {
  Image = 'image',
  SpriteSheet = 'spritesheet',
  Audio = 'audio',
}

export enum AssetExtension {
  PNG = '.png',
  MP3 = '.mp3',
}

export interface AssetConfig {
    frameWidth: number;
    frameHeight: number;
    paddingX: number;
    paddingY: number;

}

export interface ServerAsset {
    key: string;
    set(): void;
}
