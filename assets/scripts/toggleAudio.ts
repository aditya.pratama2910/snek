import {
  _decorator,
  Component,
  find,
  AudioSource,
  Toggle,
  ToggleComponent,
} from "cc";
import { PersistentNode } from "./persistentNode";
const { ccclass } = _decorator;

@ccclass("ToggleAudio")
export class ToggleAudio extends Component {

  onLoad() {
      this.node.on(Toggle.EventType.TOGGLE, this.toggleMute, this);
  }

  start() {
    const toggleComponent = this.node.getComponent(ToggleComponent);
    if(toggleComponent) {
      toggleComponent.isChecked = PersistentNode.instance.isMuted();
    }
  }

  toggleMute(toggle: ToggleComponent) {
    PersistentNode.instance.mute(!toggle.isChecked);
  }
}
