
import { _decorator, Component, Node, director, game, Toggle, AudioSource, Button, Director } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('PersistentNode')
export class PersistentNode extends Component {
    static instance: PersistentNode;

    @property({type: AudioSource})
    bgm?: AudioSource;

    start () {
        if(PersistentNode.instance) {
            this.node.destroy();

            return;
        }

        game.addPersistRootNode(this.node);
        
        PersistentNode.instance = this;

        director.on(Director.EVENT_AFTER_SCENE_LAUNCH, () => {
            this.node.getComponent(AudioSource)?.play();
        });
    }

    isMuted() {
        if(this.bgm) {
            return this.bgm.volume === 0 ? true : false;
        }
        else return true;
    }

    mute(mute: boolean) {
        if(this.bgm) {
            this.bgm.volume = mute? 1 : 0;
        }
    }

    goToScene(button: Button, sceneName: string) {
        director.loadScene(sceneName);
    }
}