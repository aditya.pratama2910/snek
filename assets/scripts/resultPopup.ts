
import { _decorator, Component, Node, Label, Button, director } from 'cc';
import { PersistentNode } from './persistentNode';
const { ccclass, property } = _decorator;

@ccclass('ResultPopup')
export class ResultPopup extends Component {
    @property({type: Label})
    scoreText?: Label;

    @property({type: Button})
    cancelButton?: Button;

    @property({type: Button})
    restartButton?: Button;

    start () {
        this.node.active = false;
        
        if(this.cancelButton) {
            this.cancelButton.node.on('click', this.onCancelPressed, this);
        }

        if(this.restartButton) {
            this.restartButton.node.on('click', this.onPlayAgainPressed, this);
        }
    }

    show(score: number) {
        this.node.active = true;
        if(this.scoreText) {
            this.scoreText.string = score.toString();
        }
    }

    private onCancelPressed() {
        director.loadScene("title");
    }

    private onPlayAgainPressed() {
        director.loadScene("game");
    }
}