import {
  _decorator,
  Component,
  Sprite,
  assetManager,
  SpriteFrame,
  find,
} from "cc";
import { ServerAsset } from "../interfaces/asset";
import { AssetLoader } from "../assetLoader";
const { ccclass, property } = _decorator;

@ccclass("BaseSprite")
export class BaseSprite extends Component implements ServerAsset {
  @property()
  waitForAssetLoader = false;

  key: string;
  protected frame?: string | number;
  protected sprite!: Sprite;

  constructor(key: string, frame?: string | number) {
    super("BaseSprite");
    this.key = key;
    this.frame = frame;
  }

  onLoad() {
    this.sprite = this.getComponent(Sprite)!;

    if (this.waitForAssetLoader) {
      const assetLoader = find("assetLoader")?.getComponent(AssetLoader);
      if (assetLoader) {
        assetLoader.addServerAsset(this);
      }
    }
    else {
        this.set();
    }
  }

  set() {
    this.sprite.spriteFrame = assetManager.assets.get(this.key) as SpriteFrame;
  }
}
