import {
  _decorator,
  Component,
  AudioSource,
  assetManager,
  AudioClip,
  find,
} from "cc";
import { ServerAsset } from "./interfaces/asset";
import { AudioKey } from "./helpers/assetKey";
import { AssetLoader } from "./assetLoader";

const { ccclass, property } = _decorator;

@ccclass("Bgm")
export class Bgm extends Component implements ServerAsset {
  key: string;
  @property({ type: AudioSource })
  bgmSource!: AudioSource;

  constructor() {
    super("BGM");
    this.key =  AudioKey.BGM;
  }

  start() {
    const assetLoader = find("assetLoader")?.getComponent(AssetLoader);
    if (assetLoader) {
      assetLoader.addServerAsset(this);
    }
  }

  set() {
    const bgmClip = assetManager.assets.get(this.key) as AudioClip;
    this.bgmSource.clip = bgmClip;
    this.bgmSource.play();
  }
}
